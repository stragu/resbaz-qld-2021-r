---
title: "About"
date: "2021-11-25"
---

This website and associated repository were adapted from [previous work](https://gitlab.com/uqlib/r-intro-biology) by Catherine Kim and Stéphane Guillou. It hosts the material for two R-related workshops presented at [ResBaz Qld 2021](https://resbaz.github.io/resbaz2021qld/).

## Licence

The material is released under a [Creative Commons - Attribution 4.0 International Licence](https://creativecommons.org/licenses/by/4.0/). You may re-use and re-mix the material in any way you wish, without asking permission, provided you cite the original source. However, we'd love to hear about what you do with it!

# Contact

Are you a member of the UQ community that is interested in a training workshop through the Library? 

Contact the training team at: training at library dot uq dot edu dot au