---
title: 'R with RStudio: getting started'
author: "Paula Andrea Martinez, Catherine Kim, Stéphane Guillou, Amir Farrokh-Niae"
date: "2021-11-25"
output:
  blogdown::html_page:
    toc: true
weight: 1
---

<script src="{{< blogdown/postref >}}index_files/header-attrs/header-attrs.js"></script>

<div id="TOC">
<ul>
<li><a href="#r-rstudio">R + RStudio</a></li>
<li><a href="#installation">Installation</a></li>
<li><a href="#open-rstudio">Open RStudio</a></li>
<li><a href="#what-are-we-going-to-learn">What are we going to learn?</a></li>
<li><a href="#r-projects">R Projects</a></li>
<li><a href="#maths-and-objects">Maths and objects</a></li>
<li><a href="#functions">Functions</a>
<ul>
<li><a href="#help">Help</a></li>
<li><a href="#more-help">More help</a></li>
</ul></li>
<li><a href="#create-a-folder-structure">Create a folder structure</a></li>
<li><a href="#scripts">Scripts</a>
<ul>
<li><a href="#many-ways-to-do-one-thing">Many ways to do one thing</a></li>
<li><a href="#comments">Comments</a></li>
<li><a href="#syntax-highlighting">Syntax highlighting</a></li>
</ul></li>
<li><a href="#import-data">Import data</a>
<ul>
<li><a href="#challenge-2-import-data">Challenge 2 – Import data</a></li>
</ul></li>
<li><a href="#explore-data">Explore data</a></li>
<li><a href="#packages">Packages</a></li>
<li><a href="#closing-rstudio">Closing RStudio</a></li>
<li><a href="#what-next">What next?</a></li>
</ul>
</div>

<div id="r-rstudio" class="section level2">
<h2>R + RStudio</h2>
<p>The <a href="https://cran.r-project.org/">R programming language</a> is a language used for calculations, statistics, visualisations and many more data science tasks.</p>
<p><a href="https://rstudio.com/products/rstudio/">RStudio</a> is an open source Integrated Development Environment (IDE) for R, which means it provides many features on top of R to make it easier to write and run code.</p>
<p>R’s main strong points are:</p>
<ul>
<li><strong>Open Source</strong>: you can install it anywhere and adapt it to your needs;</li>
<li><strong>Reproducibility</strong>: makes an analysis repeatable by detailing the process in a script;</li>
<li><strong>Customisable</strong>: being a programming language, you can create your own custom tools;</li>
<li><strong>Big data</strong>: it can handle very large datasets;</li>
<li><strong>Many formats</strong>: it can read and write most file formats;</li>
<li><strong>Large ecosystem</strong>: packages allow you to extend R for thousands of different analyses.</li>
</ul>
<p>The learning curve will be steeper than point-and-click tools, but as far as programming languages go, R is more user-friendly than others.</p>
</div>
<div id="installation" class="section level2">
<h2>Installation</h2>
<p>For this course, you need to have both R and RStudio installed (<a href="https://gitlab.com/stragu/DSH/blob/master/R/Installation.md">installation instructions</a>).</p>
</div>
<div id="open-rstudio" class="section level2">
<h2>Open RStudio</h2>
<ul>
<li><p>If you are using your own laptop please open RStudio</p>
<ul>
<li>Make sure you have a working Internet connection</li>
</ul></li>
</ul>
</div>
<div id="what-are-we-going-to-learn" class="section level2">
<h2>What are we going to learn?</h2>
<p>This session is designed to get straight into using R in a short amount of time, which is why we won’t spend too much time on the smaller details that make the language.</p>
<p>During this session, you will:</p>
<ul>
<li>Create a project for data analysis</li>
<li>Create a folder structure</li>
<li>Know where to find help</li>
<li>Learn about a few useful functions</li>
<li>Generate a data visualisation</li>
<li>Create a script</li>
<li>Import a dataset</li>
<li>Understand the different RStudio panels</li>
<li>Use a few shortcuts</li>
<li>Know how to extend R with packages</li>
</ul>
</div>
<div id="r-projects" class="section level2">
<h2>R Projects</h2>
<p>Let’s first create a new project:</p>
<ul>
<li>Click the “File” menu button (top left corner), then “New Project”</li>
<li>Click “New Directory”</li>
<li>Click “New Project”</li>
<li>In “Directory name”, type the name of your project, for example “YYYY-MM-DD_rstudio-intro”</li>
<li>Browse and select a folder where to locate your project (<code>~</code> is your home directory). For example, a folder called “r-projects”.</li>
<li>Click the “Create Project” button</li>
</ul>
<blockquote>
<p>R Projects make your work with R more straight forward, as they allow you to segregate your different projects in separate folders. You can create a .Rproj file in a new directory or an existing directory that already has R code and data. Everything then happens by default in this directory. The .Rproj file stores information about your project options, and allows you to go straight back to your work.</p>
</blockquote>
</div>
<div id="maths-and-objects" class="section level2">
<h2>Maths and objects</h2>
<p>The <strong>console</strong> (usually at the bottom right in RStudio) is where most of the action happens. In the console, we can use R interactively. We write a <strong>command</strong> and then <strong>execute</strong> it by pressing <kbd>Enter</kbd>.</p>
<p>In its most basic use, R can be a calculator. Try executing the following commands:</p>
<pre class="r"><code>10 - 2</code></pre>
<pre><code>## [1] 8</code></pre>
<pre class="r"><code>3 * 4</code></pre>
<pre><code>## [1] 12</code></pre>
<pre class="r"><code>2 + 10 / 5</code></pre>
<pre><code>## [1] 4</code></pre>
<p>Those symbols are called “binary operators”: we can use them to multiply, divide, add, subtract and exponentiate. Once we execute the command (the “input”), we can see the result in the console (the “output”).</p>
<p>What if we want to keep reusing the same value? We can store data by creating <strong>objects</strong>, and assigning values to them with the <strong>assignment operator</strong> <code>&lt;-</code>:</p>
<pre class="r"><code>num1 &lt;- 42
num2 &lt;- num1 / 9
num2</code></pre>
<pre><code>## [1] 4.666667</code></pre>
<p>We can also store text data:</p>
<pre class="r"><code>sentence &lt;- &quot;Hello World!&quot;
sentence</code></pre>
<pre><code>## [1] &quot;Hello World!&quot;</code></pre>
<p>You should now see your objects listed in you <strong>environment pane</strong> (top right).</p>
<p>As you can see, you can store different kinds of data as objects. If you want to store text data (a “string of characters”), you have to surround it by quotes.</p>
<p>Depending on the kind of data, some operations will be possible, and others will yield an error:</p>
<pre><code>&gt; sentence * 2
Error in sentence * 2 : non-numeric argument to binary operator</code></pre>
<p>In the error message above, R is telling us that using the multiplication operator <code>*</code> can only deal with numeric data. <code>sentence</code> is of “character” type.</p>
<blockquote>
<p>You can use the shortcut <kbd>Alt</kbd>+<kbd>-</kbd> to type the assignement operator quicker.</p>
</blockquote>
</div>
<div id="functions" class="section level2">
<h2>Functions</h2>
<p>An R <strong>function</strong> is a little program that does a particular job. It usually looks like this:</p>
<pre><code>&lt;functionname&gt;(&lt;argument(s)&gt;)</code></pre>
<p><strong>Arguments</strong> tell the function what to do. Some functions don’t need arguments, others need one or several, but they always need the parentheses after their name.</p>
<p>For example, try running the following command:</p>
<pre class="r"><code>round(num2)</code></pre>
<pre><code>## [1] 5</code></pre>
<p>The <code>round()</code> function rounds a number to the closest integer. The only argument we give it is <code>num2</code>, the number we want to round.</p>
<blockquote>
<p>If you scroll back to the top of your console, you will now be able to spot functions in the text.</p>
</blockquote>
<div id="help" class="section level3">
<h3>Help</h3>
<p>What if we want to learn more about a function?</p>
<p>There are two main ways to find <strong>help</strong> about a specific function in RStudio:</p>
<ol style="list-style-type: decimal">
<li>the shortcut command: <code>?functionname</code></li>
<li>the keyboard shortcut: press <kbd>F1</kbd> with your cursor in a function name</li>
</ol>
<p>Let’s look through the documentation for the <code>round()</code> function:</p>
<pre class="r"><code>?round</code></pre>
<p>As you can see, different functions might share the same documentation page.</p>
<p>There is quite a lot of information in a function’s documentation, but the most important bits are:</p>
<ul>
<li><strong>Description</strong>: general description of the function(s)</li>
<li><strong>Usage</strong>: overview of what syntax can be used</li>
<li><strong>Arguments</strong>: description of what each argument is</li>
<li><strong>Examples</strong>: some examples that demonstrate what is possible</li>
</ul>
<p>See how the <code>round()</code> function has a second argument available? Try this now:</p>
<pre class="r"><code>round(num2, digits = 2)</code></pre>
<pre><code>## [1] 4.67</code></pre>
<p>We can change the default behaviour of the function by telling it how many digits we want after the decimal point, using the argument <code>digits</code>. And if we use the arguments in order, we don’t need to name them:</p>
<pre class="r"><code>round(num2, 2)</code></pre>
<pre><code>## [1] 4.67</code></pre>
<div id="challenge-1-finding-help" class="section level4">
<h4>Challenge 1 – Finding help</h4>
<p>Use the help pages to find out what these functions do, and try executing commands with them:</p>
<ol style="list-style-type: decimal">
<li><code>c()</code></li>
<li><code>rep.int()</code></li>
<li><code>mean()</code></li>
<li><code>rm()</code></li>
</ol>
<p><code>c()</code> combines the arguments into a vector. In other words, it takes any number of arguments (hence the <code>...</code>), and stores all those values together, as one single object. For example, let’s store the ages of our pet dogs in a new object:</p>
<pre class="r"><code>ages &lt;- c(4, 10, 2, NA, 3)</code></pre>
<blockquote>
<p>You can store missing data as <code>NA</code>.</p>
</blockquote>
<p>We can now reuse this vector, and calculate their human age<a href="#fn1" class="footnote-ref" id="fnref1"><sup>1</sup></a>:</p>
<pre class="r"><code>ages * 7</code></pre>
<pre><code>## [1] 28 70 14 NA 21</code></pre>
<p>Or use it in a logical operation:</p>
<pre class="r"><code>ages &gt; 3</code></pre>
<pre><code>## [1]  TRUE  TRUE FALSE    NA FALSE</code></pre>
<p>R can create visualisations with functions too. Try a bar plot of your dogs’ ages with the <code>barplot()</code> function:</p>
<pre class="r"><code>barplot(ages)</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-10-1.png" width="672" /></p>
<p>Remember that vectors need to stick to one kind of data, so the following command:</p>
<pre class="r"><code>things &lt;- c(1, TRUE, &quot;Hi!&quot;)</code></pre>
<p>… will <em>coerce</em> all elements to one single type: the “character” type.</p>
<blockquote>
<p>Other data structures are available for storing your data in R: <strong>lists</strong> (very flexible) and <strong>data frames</strong> (like spreadsheets), as well as <strong>matrices</strong> (with one data type, like vectors). We will talk about data frames in our example project.</p>
</blockquote>
<p>Moving on to our second function: <code>rep.int()</code> also creates vectors, but it is designed to easily replicate values. For example, if you find something very funny:</p>
<pre class="r"><code>rep.int(&quot;Ha!&quot;, 30)</code></pre>
<pre><code>##  [1] &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot;
## [13] &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot;
## [25] &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot; &quot;Ha!&quot;</code></pre>
<p>The next function, <code>mean()</code>, returns the mean of a vector of numbers:</p>
<pre class="r"><code>mean(ages)</code></pre>
<pre><code>## [1] NA</code></pre>
<p>What happened there?</p>
<p>We have an NA value in the vector, which means the function can’t tell what the mean is. If we want to change this default behaviour, we can use an extra argument: <code>na.rm</code>, which stands for “remove NAs”.</p>
<pre class="r"><code>mean(ages, na.rm = TRUE)</code></pre>
<pre><code>## [1] 4.75</code></pre>
<blockquote>
<p>In our last command, if we hadn’t named the <code>na.rm</code> argument, R would have understood <code>TRUE</code> to be the value for the <code>trim</code> argument!</p>
</blockquote>
<p>Finally, <code>rm()</code> removes an object from your environment (<code>remove()</code> and <code>rm()</code> point to the same function). For example:</p>
<pre class="r"><code>rm(num1)</code></pre>
<blockquote>
<p>R does not check if you are sure you want to remove something! As a programming language, it does what you ask it to do, which means you might have to be more careful. But you’ll see later on that, when working with scripts, this is less of a problem.</p>
</blockquote>
<p>Let’s do some more complex operations by combining two functions:</p>
<p><code>ls()</code> returns a character vector: it contains the names of all the objects in the current environment (i.e. the objects we created in this R session). Is there a way we could combine it with <code>rm()</code>?</p>
<p>You can remove <em>all</em> the objects in the environment by using <code>ls()</code> as the value for the <code>list</code> argument:</p>
<pre class="r"><code>rm(list = ls())</code></pre>
<p>We are nesting a function inside another one. More precisely, we are using the output of the <code>ls()</code> function as the value passed on to the <code>list</code> argument in the <code>rm()</code> function.</p>
</div>
</div>
<div id="more-help" class="section level3">
<h3>More help</h3>
<p>We’ve practised how to find help about functions we know the name of. What if we don’t know what the function is called? Or if we want general help about R?</p>
<ul>
<li>The Home 🏠 button in the Help panel is a good starting point: it opens a browser of official R and RStudio help.</li>
<li>If you want to search for a word in the function names and the documentation titles, you can use the <code>??</code> syntax (or the 🔍 search box in the Help tab). For example, try executing <code>??anova</code>.</li>
<li>Finally, you will often go to your web browser and search for a particular question, or a specific error message: most times, there already is an answer somewhere on the Internet. The challenge is to ask the right question!</li>
</ul>
</div>
</div>
<div id="create-a-folder-structure" class="section level2">
<h2>Create a folder structure</h2>
<p>To keep it tidy, we are creating 3 folders in our project directory:</p>
<ul>
<li>scripts</li>
<li>data</li>
<li>plots</li>
</ul>
<p>For that, we use the function <code>dir.create()</code>:</p>
<pre class="r"><code>dir.create(&quot;scripts&quot;)
dir.create(&quot;data&quot;)
dir.create(&quot;plots&quot;)</code></pre>
<blockquote>
<p>You can recall your recent commands with the up arrow, which is especially useful to correct typos or slightly modify a long command.</p>
</blockquote>
</div>
<div id="scripts" class="section level2">
<h2>Scripts</h2>
<p>Scripts are simple text files that contain R code. They are useful for:</p>
<ul>
<li>saving a set of commands for later use (and executing it in one click)</li>
<li>making research reproducible</li>
<li>making writing and reading code more comfortable</li>
<li>documenting the code with comments, and</li>
<li>sharing your work with peers</li>
</ul>
<p>Let’s create a new R script with a command:</p>
<pre class="r"><code>file.create(&quot;scripts/process.R&quot;)</code></pre>
<blockquote>
<p>All the file paths are <strong>relative</strong> to our current working directory, i.e. the project directory. To use an <strong>absolute</strong> file path, we can start with <code>/</code>.</p>
</blockquote>
<p>To edit the new script, use the <code>file.edit()</code> function. Try using the <kbd>Tab</kbd> key to autocomplete your function name and your file path!</p>
<pre class="r"><code>file.edit(&quot;scripts/process.R&quot;)</code></pre>
<p>This opens our fourth panel in RStudio: the <strong>source panel</strong>.</p>
<div id="many-ways-to-do-one-thing" class="section level3">
<h3>Many ways to do one thing</h3>
<p>As in many programs, there are many ways to achieve one thing.</p>
<p>For example, we used commands to create and edit a script, but we could also:</p>
<ul>
<li>use the shortcut <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>N</kbd></li>
<li>use the top left drop-down menus</li>
</ul>
<p>Learning how to use functions rather than the graphical interface will allow you to integrate them in scripts, and will sometimes help you to do things faster.</p>
</div>
<div id="comments" class="section level3">
<h3>Comments</h3>
<p>We should start with a couple of <strong>comments</strong>, to document our script. Comments start with <code>#</code>, and will be ignored by R:</p>
<pre class="r"><code># Description: Introduction to R and RStudio
# Author: &lt;your name&gt;
# Date: &lt;today&#39;s date&gt;</code></pre>
</div>
<div id="syntax-highlighting" class="section level3">
<h3>Syntax highlighting</h3>
<p>Now, add a command to your script:</p>
<pre class="r"><code>vect &lt;- c(3, &quot;Hi!&quot;)</code></pre>
<p>Notice the colours? This is called <strong>syntax highlighting</strong>. This is one of the many ways RStudio makes it more comfortable to work with R. The code is more readable when working in a script.</p>
<blockquote>
<p>While editing your script, you can run the current command (or the selected block of code) by using <kbd>Ctrl</kbd>+<kbd>Enter</kbd>. Remember to save your script regularly with the shortcut <kbd>Ctrl</kbd>+<kbd>S</kbd>. You can find more shortcuts with <kbd>Alt</kbd>+<kbd>Shift</kbd>+<kbd>K</kbd>, or the menu “Tools &gt; Keyboard Shortcuts Help”.</p>
</blockquote>
</div>
</div>
<div id="import-data" class="section level2">
<h2>Import data</h2>
<div id="challenge-2-import-data" class="section level3">
<h3>Challenge 2 – Import data</h3>
<p>Copy and paste the following two commands into your script:</p>
<pre class="r"><code>download.file(url = &quot;https://raw.githubusercontent.com/resbaz/r-novice-gapminder-files/master/data/gapminder-FiveYearData.csv&quot;,
  destfile = &quot;data/gapminderdata.csv&quot;)
gapminder &lt;- read.csv(&quot;data/gapminderdata.csv&quot;)</code></pre>
<p>What do you think they do? Describe each one in detail, and try executing them.</p>
</div>
</div>
<div id="explore-data" class="section level2">
<h2>Explore data</h2>
<p>We have downloaded a CSV file from the Internet, and read it into an object called <code>gapminder</code>.</p>
<p>You can type the name of your new object to print it to screen:</p>
<pre class="r"><code>gapminder</code></pre>
<p>That’s a lot of lines printed to your console. To have a look at the first few lines only, we can use the <code>head()</code> function:</p>
<pre class="r"><code>head(gapminder)</code></pre>
<pre><code>##       country year      pop continent lifeExp gdpPercap
## 1 Afghanistan 1952  8425333      Asia  28.801  779.4453
## 2 Afghanistan 1957  9240934      Asia  30.332  820.8530
## 3 Afghanistan 1962 10267083      Asia  31.997  853.1007
## 4 Afghanistan 1967 11537966      Asia  34.020  836.1971
## 5 Afghanistan 1972 13079460      Asia  36.088  739.9811
## 6 Afghanistan 1977 14880372      Asia  38.438  786.1134</code></pre>
<p>Now let’s use a few functions to learn more about our dataset:</p>
<pre class="r"><code>class(gapminder) # what kind of object is it stored as?</code></pre>
<pre><code>## [1] &quot;data.frame&quot;</code></pre>
<pre class="r"><code>nrow(gapminder) # how many rows?</code></pre>
<pre><code>## [1] 1704</code></pre>
<pre class="r"><code>ncol(gapminder) # how many columns?</code></pre>
<pre><code>## [1] 6</code></pre>
<pre class="r"><code>dim(gapminder) # rows and columns</code></pre>
<pre><code>## [1] 1704    6</code></pre>
<pre class="r"><code>names(gapminder) # variable names</code></pre>
<pre><code>## [1] &quot;country&quot;   &quot;year&quot;      &quot;pop&quot;       &quot;continent&quot; &quot;lifeExp&quot;   &quot;gdpPercap&quot;</code></pre>
<p>All the information we just saw (and more) is available with one single function:</p>
<pre class="r"><code>str(gapminder) # general structure</code></pre>
<pre><code>## &#39;data.frame&#39;:    1704 obs. of  6 variables:
##  $ country  : chr  &quot;Afghanistan&quot; &quot;Afghanistan&quot; &quot;Afghanistan&quot; &quot;Afghanistan&quot; ...
##  $ year     : int  1952 1957 1962 1967 1972 1977 1982 1987 1992 1997 ...
##  $ pop      : num  8425333 9240934 10267083 11537966 13079460 ...
##  $ continent: chr  &quot;Asia&quot; &quot;Asia&quot; &quot;Asia&quot; &quot;Asia&quot; ...
##  $ lifeExp  : num  28.8 30.3 32 34 36.1 ...
##  $ gdpPercap: num  779 821 853 836 740 ...</code></pre>
<blockquote>
<p>The RStudio Environment panel already shows us some of that information (click on the blue arrow next to the object name).</p>
</blockquote>
<p>And to explore the data in a viewer, run the following:</p>
<pre class="r"><code>View(gapminder) # spreadsheet-like view in new tab</code></pre>
<p>This viewer allows you to explore your data by scrolling through, searching terms, filtering rows and sorting the data. Remember that it is only a viewer: it will never modify your original object.</p>
<p>Notice that in R, the case matters: trying to use <code>view()</code> (with a lowercase “V”) will yield an error.</p>
<blockquote>
<p>You can also click on the spreadsheet icon in your Environment pane to open the viewer.</p>
</blockquote>
<p>To see summary statistics for each of our variables, you can use the <code>summary()</code> function:</p>
<pre class="r"><code>summary(gapminder)</code></pre>
<pre><code>##    country               year           pop             continent        
##  Length:1704        Min.   :1952   Min.   :6.001e+04   Length:1704       
##  Class :character   1st Qu.:1966   1st Qu.:2.794e+06   Class :character  
##  Mode  :character   Median :1980   Median :7.024e+06   Mode  :character  
##                     Mean   :1980   Mean   :2.960e+07                     
##                     3rd Qu.:1993   3rd Qu.:1.959e+07                     
##                     Max.   :2007   Max.   :1.319e+09                     
##     lifeExp        gdpPercap       
##  Min.   :23.60   Min.   :   241.2  
##  1st Qu.:48.20   1st Qu.:  1202.1  
##  Median :60.71   Median :  3531.8  
##  Mean   :59.47   Mean   :  7215.3  
##  3rd Qu.:70.85   3rd Qu.:  9325.5  
##  Max.   :82.60   Max.   :113523.1</code></pre>
<p>Notice how categorical and numerical variables are handled differently?</p>
<p>Finally, let’s plot the relationship between GDP per capita and life expectancy:</p>
<pre class="r"><code>plot(gapminder$gdpPercap, gapminder$lifeExp,
     xlab = &quot;GDP per capita (USD)&quot;,
     ylab = &quot;Life expectancy (years)&quot;)</code></pre>
<p><img src="{{< blogdown/postref >}}index_files/figure-html/unnamed-chunk-24-1.png" width="672" /></p>
</div>
<div id="packages" class="section level2">
<h2>Packages</h2>
<p>Packages add functionalities to R and RStudio. There are more than 18,000 available on the official repository: the <a href="https://cran.r-project.org/web/packages/index.html">Comprehensive R Archive Network</a>, or “CRAN”.</p>
<p>You can see the list of installed packages in your “Packages” tab, or by using the <code>library()</code> function without any argument.</p>
<p>We are going to install and load a new package called “praise”. We can do that with the GUI: click the “Install” button in the “Packages” tab (bottom right pane), and search for “praise”.</p>
<p>Notice how it runs an <code>install.packages()</code> command in the console? You can use that too.</p>
<p>If I try running the command <code>praise()</code>, I get an error. That’s because, even though the package is installed, I need to <strong>load</strong> it every time I start a new R session. The <code>library()</code> function can do that.</p>
<pre class="r"><code>library(praise) # load the package
praise() # use a function from the package</code></pre>
<pre><code>## [1] &quot;You are amazing!&quot;</code></pre>
<p>Even though you might need the motivation provided by this function, other packages are more useful for your work.</p>
<p>Let’s take the dplyr package: it is a package that contains a number of function to transform data frames.</p>
<p>Let’s first install dplyr on our computer. In the console, execute the following command:</p>
<pre class="r"><code>install.packages(&quot;dplyr&quot;)</code></pre>
<p>This will take more time than for the praise package, as it is a bigger package that also depends on many other packages.</p>
<p>We can then load the package, and use the <code>filter()</code> function to focus on one part our <code>gapminder</code> dataset:</p>
<pre class="r"><code>library(dplyr)</code></pre>
<pre><code>## 
## Attaching package: &#39;dplyr&#39;</code></pre>
<pre><code>## The following objects are masked from &#39;package:stats&#39;:
## 
##     filter, lag</code></pre>
<pre><code>## The following objects are masked from &#39;package:base&#39;:
## 
##     intersect, setdiff, setequal, union</code></pre>
<pre class="r"><code>filter(gapminder, country == &quot;Australia&quot;)</code></pre>
<pre><code>##      country year      pop continent lifeExp gdpPercap
## 1  Australia 1952  8691212   Oceania  69.120  10039.60
## 2  Australia 1957  9712569   Oceania  70.330  10949.65
## 3  Australia 1962 10794968   Oceania  70.930  12217.23
## 4  Australia 1967 11872264   Oceania  71.100  14526.12
## 5  Australia 1972 13177000   Oceania  71.930  16788.63
## 6  Australia 1977 14074100   Oceania  73.490  18334.20
## 7  Australia 1982 15184200   Oceania  74.740  19477.01
## 8  Australia 1987 16257249   Oceania  76.320  21888.89
## 9  Australia 1992 17481977   Oceania  77.560  23424.77
## 10 Australia 1997 18565243   Oceania  78.830  26997.94
## 11 Australia 2002 19546792   Oceania  80.370  30687.75
## 12 Australia 2007 20434176   Oceania  81.235  34435.37</code></pre>
<p>This command uses a logical operation to only keep the rows of data that have the value <code>"Australia"</code> in the column <code>country</code>.</p>
</div>
<div id="closing-rstudio" class="section level2">
<h2>Closing RStudio</h2>
<p>You can close RStudio after making sure that you saved your script.</p>
<p>When you create a <strong>project</strong> in RStudio, you create an .Rproj file that gathers information about the state of your project. When you close RStudio, you have the option to save your <strong>workspace</strong> (i.e. the objects in your <strong>environment</strong>) as an .Rdata file. The .Rdata file is used to reload your workspace when you open your project again. Projects also bring back whatever source file (e.g. script) you had open, and your command history. You will find your command history in the “History” tab (upper right panel): all the commands that we used should be in there.</p>
<p>If you have a script that contains all your work, it is a good idea <em>not</em> to save your workspace: it makes it less likely to run into errors because of accumulating objects. The script will allow you to get back to where you left it, by executing all the clearly laid-out steps.</p>
<p>The console, on the other hand, only shows a brand new R <strong>session</strong> when you reopen RStudio. Sessions are not persistent, and a clean one is started when you open your project again, which is why you have to load any extra package your work requires again with the <code>library()</code> function.</p>
</div>
<div id="what-next" class="section level2">
<h2>What next?</h2>
<ul>
<li>Join this afternoon’s R + OpenStreetMap session</li>
<li>We have a <a href="https://gitlab.com/stragu/DSH/-/blob/master/R/usefullinks.md">compilation of resources</a> for the rest of your R learning (some of it UQ-specific)</li>
<li>And a cheatsheet of <a href="https://gitlab.com/stragu/DSH/-/blob/master/R/terminology.md">main terms and concepts for R</a></li>
</ul>
</div>
<div class="footnotes">
<hr />
<ol>
<li id="fn1"><p>a very scientifically correct calculation<a href="#fnref1" class="footnote-back">↩︎</a></p></li>
</ol>
</div>
