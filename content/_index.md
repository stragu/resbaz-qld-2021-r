# ResBaz Qld 2021 – R Workshops

For easy collaboration, we have a **workshop Etherpad** for today: https://etherpad.wikimedia.org/p/resbazqld2021-r

Below is the **material** for today's two R sessions, but please keep them for future reference, and try to follow along the live-coding!

The dplyr material is here in case you want to learn more about the package, but we will not look at it in detail.